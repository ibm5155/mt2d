/*===================================================================
Io_FILE.h / Io_FILE.cpp created by Lucas Zimerman Fraulob

This is the main function related to files, checking if files exist
loading files,...
====================================================================*/
#include <stdio.h>

bool check_if_file_exist(char *file);//This one open the file and check if it exist
bool check_adress_basic_test(char *string);//Check if the string looks like a address string (dont test if file exist)
FILE *open_file(char *file_name, char *acess_type);//Open a file, return a file pointer if exist, return 0 if don't, beep if not found.
bool transfer_data(char *string_data,char *destination);//Copy a string for a file + Generic gui transfer data
char *load_string(FILE *file);
/*==============================================================================================
									Keyboard.cpp / Keyboard.h 

								Created By Lucas Zimerman Fraulob
	With this code you'll be able to make use of the Keyboard for your software on an universal way
	For now MS-DOS, Windows and linux had their keyboard code standardized, for key codes, check getch output
	on Windows or the ansi struct.
	NOTE: there's also an experimental SDL code for the keyboard part, so it may not propertly work for now

	List of functions and what they do and their returns:
	
	-Keynboard_touched()
		Description: Check if the keyboard was touched (good for loops not getting stucked on keyboard_keytouched.
		Return: 1 if the keyboard was touched, 0 if the keyboard wasn't touched.

	-Keyboard_keytouched()
		Description:	Return what key was pressed, if none was pressed, it'll wait for a key,
			also, if a special key was pressed like _key, this function will make two returns, one indicating that
			_key was pressed and then witch _key was pressed.
		Return: Key pressed (ansi char)
==============================================================================================*/
bool Keyboard_touched();
int	Keyboard_keytouched();

#define _key 224
#define key_up 72
#define key_down 80
#define key_left 75
#define key_right 77
#define back_space 8

#define enter 13
#define tab 9
//#ifdef __MSDOS__
#define esc 27
//#else
//#define esc 1
//#endif

//#define � 135


/* THIS FILE INCLUDE ALL THE MT2D FILES
-Object.h not included since depending of witch software are you going to make
you'll not need it.
============================================*/

#include "Window_Core.h"//where the buffers and basic print clear screen functions are
#include "System_Calls.h"//where system functions are, like delay, beep,...
#include "Keyboard.h"     //because you need keyboard...
#include "Io_FILE.h"      //some ansi and non ansi functions

//building functions here too since, most of the time you'll use it

#include "building_functions/display_popup.h"
#include "building_functions/display_popup_with_result.h"
#include "building_functions/generic_line.h"
#include "building_functions/generic_menu.h"
#include "building_functions/generic_String_reader.h"
#include "building_functions/generic_transfer_file.h"//will be renamed to generic_file_transfer on the future...
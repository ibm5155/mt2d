==============================================================================
                     MONOCHROME TEXT 2D ENGINE

==============================================================================

 ALL THIS FILES WERE WRITTEN ON ANSI_WORD.

1.What is MT2D
2.What operational system would it work?
3.Basics
4.How to compile
5.Contribuitors
6.Demo Projects


1)What is MT2D
 As the title say:Monochrome text 2D engine, it's an api where you use the
console as a interface (just like that old DOS interfaces).
 As the name say, there's no color, and there's no size change, it's just the
basic 24x80 screen.
 Some cool features build in are the object functions, where it's abble to cre
ate some simple games, and the popup system.

2)What operational system would it work?
 Since 95% of MT2D code is made in C ANSI, it's abble to be compiled in any
system, but, since there're some non ansi code, it'll be required to be made
some little changes to be compiled, for now, MT2D is able to be compiled into
Windows and MS-DOS.

3)Basics.
 The main part of MT2D is a text buffer, with that you can make any thing you
want, not requiring other functions, but there're some cool build in functions
 like:

a)Keyboard:
-Here you'll find almost all functions related to keyboard, like if keyboard
 was touched or witch key was touched...

b)Io_FILE:
-Most functions related to files are here, like checking is an address is valid or if a file exist,...

c)System Calls:
-Here you'll have some specific system functions, like beep, wait,..

d)Window Core:
-Here are the main functions of MT2D, like put the buffer on screen, clear the
 buffer, change cursor to the top, put a string on the buffer, transfer buffer
 to another,...

Plus there are some extra functions on building_functions folder, where 
you'll find some cool functions like

print popup: show a message on screen
print popup with result: show a message on screen and return a selected value
generic line:draw a line on buffer
generic menu:draw a working menu with x inputs
generic string reader: a popup that return a string
generic transfer file:a popup where it transfers x files to a specific folder
(still not working 100%)

Also, a cool feature for gamers developers is the Object
-It's a function where you're able to create objects, store ASCII arts, manage
 ASCII "sprites", draw objects on buffer, control health, aceleration,....

4)How to Compile

Tested softwares (Visual Studio, Borland C++, it'll work in other softwares) basically, Include all the files as a normal .c .cpp file, add the headers and c files to your project and you're ready to start :D
the main file from MT2D is window_core.h, so you better include it into your main file.

5)Contribuitors

IBM5155: Admin, project leader, code, test,...

6)Demo Projects

To be released...
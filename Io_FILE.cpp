#ifdef __MSDOS__
#include "..\..\mt2d\Io_FILE.h"
#else
#include "Io_FILE.h"
#endif
#include <stdlib.h>


bool check_adress_basic_test(char *string){//Simple check to see if the user didnt type some unit with a number and not a letter,...
	bool out=false;
	if(string[0]>=65 && string[0]<=122){
		if(string[0]<=90 || string[0]>=97){
			if(string[1]==58){
				if(string[2]==92) out=true;
			}
		}
	}
	return out;
}

FILE *open_file(char *file_name, char *acess_type){
	FILE *_x;
	_x=fopen(file_name,acess_type);
	if(!_x)printf("\a");
	return _x;
}


bool check_if_file_exist(char *file){
	FILE *x;
	bool output=true;
	x=open_file(file,(char*)"r");
	if(!x) output=false;
	else fclose(x);
	return output;
}

void save_file_ansichar(FILE *file_opened,char x){

	fputc(x,file_opened);
}

// REFAZER TRANSFER DATA
bool transfer_data(char *string_data,char *destination){
	FILE *x;
	if(check_if_file_exist(destination)==NULL){
		x=fopen(destination,"w");
		fprintf(x,"%s",string_data);
	}else{
		x=fopen(destination,"w");
		fprintf(x,"%s",string_data);

	}
	return 0;
}

char *load_string(FILE *file){
	char *x=(char*)malloc(sizeof(char));
	char buff;
	int i=0;
	fseek(file,0,SEEK_SET);
	while(!feof(file)){
		buff=fgetc(file);
		x=(char*)realloc(x,(i+1)*sizeof(char));
		x[i]=buff;
		i++;
	};
	x[i-1]='\0';
	fseek(file,0,SEEK_SET);
	return x;
}

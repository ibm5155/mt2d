#ifdef __MSDOS__
#include "..\..\mt2d\Keyboard.h"
#include "..\..\mt2d\Window_core.h"
#else
#include "../Keyboard.h"
#include "../Window_core.h"
#endif
#include <stdio.h>
#include <string.h>

extern unsigned char WINDOW1[MAX_VER+1][MAX_HOR];
extern unsigned char WINDOW2[MAX_VER+1][MAX_HOR];

void print_popup(char *mensage){
	int i=0,j=0,mensage_size;
	int pos_hor=4,pos_ver=10;
	int win_hor=3,win_ver=9;
	bool out_loop=false;
	mensage_size=strlen(mensage);
	printf("\a");
	while(out_loop==false){
		i=0;
		j=0;
		pos_hor=win_hor+1;
		pos_ver=win_ver+1;
		while(i<MAX_VER	){
			while(j<MAX_HOR){
				WINDOW2[i][j]=WINDOW1[i][j];
				j++;
			}
			i++;
			j=0;
		}
		i=4;
		create_window_layout(win_hor,win_ver,73,4,true,false,false,' ',DISPLAY_WINDOW2);
		j=0;
		while(j<mensage_size){//Put the mensage on the window
			if(pos_hor==win_hor+70){pos_hor=win_hor+1;pos_ver++;}
			if(pos_ver==win_ver+4 )break;
			WINDOW2[pos_ver][pos_hor]=mensage[j];
			j++;
			pos_hor++;
		}
		print_display(DISPLAY_WINDOW2);
		i=Keyboard_keytouched();
		if(i==_key){//MOVE THE WINDOW POSITION
			i=Keyboard_keytouched();
			switch(i){
			case key_up:
				if(win_ver>0)win_ver--;
				out_loop=false;
				break;
			case key_down:
				if(win_ver<MAX_VER-5)win_ver++;
				out_loop=false;
				break;
			case key_left:
				if(win_hor>0)win_hor--;
				out_loop=false;
				break;
			case key_right:
				if(win_hor<MAX_HOR-74)win_hor++;
				out_loop=false;
				break;
			}
		}
		else out_loop=true;
	}
}

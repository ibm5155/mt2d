/*====================================================================
generic_line.h / generic_line.cpp created By Lucas Zimerman Fraulob

This file will draw a horizontal or vertical line at window1 and the 
line char you select what will be
======================================================================*/

void line_draw_vertical(int pos, int type);
void line_draw_horizontal(int pos, int type);
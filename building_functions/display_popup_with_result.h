/*=====================================================================
display_popup.h / display_popup.cpp created By Lucas Zimerman Fraulob

This file will contain a simple popup system on MT2D engine that have
two options and will return a value (1 or 2)
=======================================================================*/


int print_popup_wiht_result(char *error_mensage_title,char *error_mensage, char *option_1, char *option_2);//Print a generic error box with two options to be selected
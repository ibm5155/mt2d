/*===================================================================
Window_core.h/ Window_core.cpp created By Lucas Zimerman Fraulob

This file will contain the basic MT2D windows display system core

	List of functions and what they do and their returns:
	-bool MT2D_Init()
		Start the inital MT2D struct (mostly start each specific os/api struct)
	-clear_display()
		Description: Clears the WINDOW1 buffer, not the display, it's not the best name and will be replaced
		on the future.
		Return: none
	-Print_Display()
		Description: Takes all the video buffer on WINDOW1 or WINDOW2 and put it on the terminal or screen memory
		Return: none
	-goto_topscreen()
		Description: Move the terminal blink position to the top of the screen (WINDOWS ONLY)
		Return: none
	-change_cursor_position()
		Description: Change the cursor position to any valid place (WINDOWS ONLY)
		Return: none
	-create_window_layout()
		Description: Draw a Window with specific position, size and others elements on it on WINDOW1 or WINDOW2
		Buffer.
		Return: none
	-transfer_window1_to_window2()
		Description: Transfer the WINDOW1 buffer to WINDOW2
		Return: none
	-transfer_window2_to_window1()
		Description: Transfer the WINDOW2 buffer to WINDOW1
		Return: none
	-insert_string_on_display()
		Description: Write a string on a specific location of WINDOW1 or WINDOW2.
		Return: none
	-insert_number_on_display()
		Description: Write a number on a specific location of WINDOW1 or WINDOW2 (integer only)
		Return: none
======================================================================*/

/** === HOW TO MT2D WITH LINUX: (tested with ubuntu)
on terminal:
sudo apt-cache search libsdl2-image
sudo apt-cache search libsdl2-dev
sudo apt-get install libsdl2-dev
sudo apt-get install libsdl2-image-dev
sudo apt-get install libsdl2-mixer-dev

linker settings: (In case of codeblocks: project -> build options -> linker settings -> other linker settings
-lSDL2
-lSDL2_image
-lSDL2_mixer

*/


#ifndef __MSDOS__
    #include "SDL_use.h"
#endif
#ifdef SDL_USE //SDL2.lib SDL2main.lib SDL2_image.lib SDL2_ttf.lib
	#ifdef WIN32
	#include <SDL.h>
	#elif defined linux
    #include <SDL2/SDL.h>
    #endif
#endif
#ifdef WIN32
#ifdef SDL_USE
#undef main
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "SDL2_image.lib")
//#pragma comment(lib, "SDL2_ttf.lib")
#pragma comment(lib, "SDL2_mixer.lib")
#endif

#endif

#define DISPLAY_WINDOW1 1
#define DISPLAY_WINDOW2 0
#define MAX_HOR 80
#define MAX_VER 25
#define MT2D_WINDOWED_MODE // Can be disabled : set the initial window size, if not defines it starts in fullscreen mode

bool MT2D_Init();
void clear_display();//Clear WINDOW1 data
void print_display(int which);//Print WINDOW1 or WINDOW2 on screen
void goto_topscreen();//Move the write pointer to the top of the screen
void change_cursor_position(int line, int column);
void create_window_layout(int pos_x, int pos_y, int tam_x, int tam_y, bool shadow, bool title, bool option, char background, int window);//Create a window box, it can be made at window1 or 2, and you can put some extras on it
void transfer_window1_to_window2();
void transfer_window2_to_window1();
void escreve_string(char *x, int pos_x,int pos_y, bool tipo);//SUBSTITUIR
void insert_string_on_display(char *x, int pos_ver, int pos_hor, bool which);
void insert_number_on_display(int number, int pos_y,int pos_x, bool display);

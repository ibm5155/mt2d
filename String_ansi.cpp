#ifdef __MSDOS__
#include "..\..\mt2d\String_ansi.h"
#include "..\..\mt2d\String_ansi_struct.h"
#include <string.h>
#endif
#ifdef WIN32
#include <string>
#include "String_ansi_struct.h"
#endif
#ifdef __GNUC__
#include <string.h>
#include "String_ansi_struct.h"
#endif
#include <stdio.h>
#include <stdlib.h>

char *join_string(char *string1, char *string2){
	char *new_string;
	int size1=0;
	if(string1)size1=strlen(string1);
	int size2=0;
	if(string2)size2=strlen(string2);
	int new_size=size1+size2-1;// there will be two end files, and the new file will have just one
	new_string=(char*)malloc(new_size*sizeof(char));
	int i=0,j=0; //letters like i, u, l, are used many times for loops and some transfer datas...
	if(!new_string){
		printf("FATAL ERROR WHILE TRYING TO ALLOC %d BYTES, ABORTING SOFTWARE!!!\n",new_size*sizeof(char));
		system("pause");
		exit(-1);
	}while(i<size1){
		new_string[i]=string1[i];
		i++;
	}while(j<size2){
		new_string[i]=string2[j];
		i++;
		j++;
	}
	new_string[i]='\0';
	return new_string;
}

STRING *create_string(){
	STRING *x;
	x=(STRING*)malloc(sizeof(STRING));
	if(x){
		x->data=(char*)malloc(sizeof(char));
		if(!x->data)printf("\a\a");
		x->size=0;
	}else printf("\a");
	return x;
}

int string_size(char *string){
 return strlen(string);
}

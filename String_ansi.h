/*==================================================================
 String_ansi.h / Stirng_ansi.cpp created by Lucas Zimerman Fraulob

 This function have some userful features about strings, like join strings...
	Join_string:
		Description: join two strings into one
		Return: string1 + string2
	Convert char to ansi (Function doesn't exist anymore)
	Convert ansi to char (Function doesn't exist)
	String Size
		Description: receive a string and get his size
		Return: size of string
====================================================================*/


char *join_string(char *string1, char *string2);
char convert_char_to_ansi(char x);
char convert_ansi_to_char(char x);
int string_size(char *string);
//STRING *create_string();
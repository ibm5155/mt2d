// MT2D Created By Lucas Zimerman Fraulob
// This is the core of MT2D engine, with only this .cpp you can do everything
// This file is related to almost all MT2D video interface, like draw, clear and update.

#include <stdio.h>

#ifndef __MSDOS__
    #include "SDL_use.h"
#endif

#ifdef SDL_USE
 //   #include "SDL_MT2D/SDL_Defines.h"
    #include "SDL_MT2D/SDL_MT2Dmain.h"
	#include "Window_core.h"
#elif defined _WIN32
	#include <Windows.h>
	#include "Window_core.h"
#elif defined __MSDOS__
	#include <dos.h>
	#include "..\..\mt2d\Window_core.h"
#elif defined linux
    #include <cstdlib>
    #include <curses.h>
    #include <signal.h>
    #include <locale.h>
	#include "Window_core.h"
#endif
#include <string.h>

unsigned char WINDOW1[MAX_VER + 1][MAX_HOR];
unsigned char WINDOW2[MAX_VER + 1][MAX_HOR];




#if defined(_WIN32) && !defined(SDL_USE) //starting data for windows only and, if windows mode, only load the terminal stuff if not using SDL
SMALL_RECT windowSize = { 0, 0, MAX_HOR - 1, MAX_VER - 1 };
COORD bufferSize = { MAX_HOR, MAX_VER };
COORD characterBufferSize = { MAX_HOR, MAX_VER };
COORD characterPosition = { 0, 0 };
SMALL_RECT consoleWriteArea = { 0, 0, MAX_HOR - 1, MAX_VER - 1 };
CHAR_INFO consoleBuffer[MAX_HOR * MAX_VER];
HANDLE wHnd; /* write (output) handle */
HANDLE rHnd; /* read (input handle */
#elif defined linux
    static void finish(int sig);
#endif



bool MT2D_Init() {// Return: true - it started without any kind of problem, false : there were a problem when MT2D was started
	bool output = true;
	#ifdef SDL_USE
	 output = init();//Start SDL files and other things
	#else
		#ifndef _DEBUG
			#ifdef _WIN32// hides the console on release projects
				HWND windowHandle = GetConsoleWindow();
				#ifdef SDL_USE
					output = ShowWindow(windowHandle, SW_HIDE);
				#endif
			#endif//if there's a way of doing the same thing, I'm going to make it
		#endif
	#endif

	#if defined(_WIN32) || defined(__MSDOS__)
		char WINDOW1_ENDMEMORY = '\0';// FIX SYSTEMS THAT USE PRINTF TO NOT PRINT TRASH
		char WINDOW2_ENDMEMORY = '\0';// FIX SYSTEMS THAT USE PRINTF TO NOT PRINT TRASH
	#endif

	return output;
}

void clear_display(){
	#ifdef __MSDOS__
	unsigned int x,y,offset;
	for(x=0;x<80;x++){
		for(y=0;y<=24;y++){//should be 25 but the windows code isn't ready for that
			WINDOW1[y][x]=' ';
		}
	}
	#elif defined _WIN32
		int i=0,j=0;
		while(i<=MAX_VER){
			while(j<MAX_HOR){
				WINDOW1[i][j]=' ';
				j++;
			}
			i++;
			j=0;
		}
		WINDOW1[MAX_VER][MAX_HOR]='\0';
		//#endif
	#elif defined linux
	int i=0,j=0;
	while(i<=MAX_VER){
		while(j<MAX_HOR){
			WINDOW1[i][j]=' ';
			j++;
		}
		i++;
		j=0;
	}
	#endif
}


void print_display(int which){
#ifdef SDL_USE
	int i=0;
	int j=0;
//	Clean_Render();
	if(which==DISPLAY_WINDOW1){
		Render_New(WINDOW1);
	}
	else{
		Render_New(WINDOW2);
		}
	SDL_Render();

#elif defined __MSDOS__
	unsigned int x,y,offset;
	if(which==DISPLAY_WINDOW1){//code that will save directly into video memory
		for(x=0;x<=MAX_HOR;x++){
			for(y=0;y<=MAX_VER;y++){//should be 25 but the windows code isn't ready for that
				offset = (80*y + x) * 2;
				pokeb(0xB800,offset,WINDOW1[y][x]);
			}
		}
	}else{
		for(x=0;x<=MAX_HOR;x++){
			for(y=0;y<=MAX_VER;y++){//should be 25 but the windows code isn't ready for that
				offset = (80*y + x) * 2;
				pokeb(0xB800,offset,WINDOW2[y][x]);
			}
		}
	}
	//transfer window1 or 2 memory to video memory, also include other systems that have video memory acess here too
	//"ansi" function that will work with any operational system
#elif defined  _WIN32
  int x, y;
  bool buffer_started =0;

  if(!buffer_started){
      wHnd = GetStdHandle(STD_OUTPUT_HANDLE);
      rHnd = GetStdHandle(STD_INPUT_HANDLE);
      SetConsoleWindowInfo(wHnd, TRUE, &windowSize);
      SetConsoleScreenBufferSize(wHnd, bufferSize);
      buffer_started=true;
  }
    if(which==DISPLAY_WINDOW1){
          for (y = 0; y < MAX_VER; ++y)
          {
            for (x = 0; x < MAX_HOR; ++x)
            {
              consoleBuffer[x + MAX_HOR * y].Char.AsciiChar = (unsigned char)WINDOW1[y][x];//insere o caracter
              consoleBuffer[x + MAX_HOR * y].Attributes = 7;//rand() % 255;//cor do caracter + cor de fundo
            }
          }
    }else{
          for (y = 0; y < MAX_VER; ++y)
          {
            for (x = 0; x < MAX_HOR; ++x)
            {
              consoleBuffer[x + MAX_HOR * y].Char.AsciiChar = (unsigned char)WINDOW2[y][x];//insere o caracter
              consoleBuffer[x + MAX_HOR * y].Attributes = 7;//rand() % 255;//cor do caracter + cor de fundo
            }
          }
    }
    WriteConsoleOutputA(wHnd, consoleBuffer, characterBufferSize, characterPosition, &consoleWriteArea);//troca o buffer do console para o novo buffer
#elif defined  linux
    int x,y;
     if(!ncurses_started){
        (void) signal (SIGINT, finish);
        (void) initscr();
        keypad (stdscr, TRUE);
        (void) nonl();
        (void) cbreak();
        (void) echo();
        setlocale(LC_ALL, "");
        ncurses_started=true;
     }

     if(which==DISPLAY_WINDOW1){
        for(y=0;y<24;y++){
            for(x=0;x<80;x++){
                move(y,x);
                if(WINDOW1[y][x]<=127)
                    addch(WINDOW1[y][x]);
                else{// no codepage 850 ;--;
                    if(WINDOW1[y][x]>=176 && WINDOW1[y][x]<=178)
                        addch(ACS_CKBOARD);
                    else if(WINDOW1[y][x]==106 || WINDOW1[y][x]==205)
                        addch(ACS_HLINE);
                    else if(WINDOW1[y][x]==179 || WINDOW1[y][x]==186)
                        addch(ACS_VLINE);
                    else if(WINDOW1[y][x]>=219 && WINDOW1[y][x]<=223)
                        addch(ACS_CKBOARD);
                    else if(WINDOW1[y][x]==254)
                        addch(ACS_CKBOARD);
                    else if(WINDOW1[y][x]==192 || WINDOW1[y][x]==200)
                        addch(ACS_LLCORNER);
                    else if(WINDOW1[y][x]==201 || WINDOW1[y][x]==218)
                        addch(ACS_ULCORNER);
                    else if(WINDOW1[y][x]==187 || WINDOW1[y][x]==191)
                        addch(ACS_URCORNER);
                    else if(WINDOW1[y][x]==188 || WINDOW1[y][x]==217)
                        addch(ACS_LRCORNER);
                    else if(WINDOW1[y][x]==204 || WINDOW1[y][x]==195)
                        addch(ACS_LTEE);
                    else if(WINDOW1[y][x]==180 || WINDOW1[y][x]==185)
                        addch(ACS_RTEE);
                    else if(WINDOW1[y][x]==193 || WINDOW1[y][x]==202)
                        addch(ACS_BTEE);
                    else if(WINDOW1[y][x]==194 || WINDOW1[y][x]==203)
                        addch(ACS_TTEE);
                    else if(WINDOW1[y][x]==197 || WINDOW1[y][x]==206)
                        addch(ACS_PLUS);
                    else if(WINDOW1[y][x]==167)
                        addch(ACS_DEGREE);
                    else if(WINDOW1[y][x]==241)
                        addch(ACS_PLMINUS);
                    else if(WINDOW1[y][x]==249 || WINDOW1[y][x]==250)
                        addch(ACS_BULLET);
                    else if(WINDOW1[y][x]==243)
                        addch(ACS_LEQUAL);
                    else if(WINDOW1[y][x]==242)
                        addch(ACS_GEQUAL);
                    else if(WINDOW1[y][x]==227)
                        addch(ACS_PI);
                    else
                        addch(32);
                }
            }
        }
     }else{
         for(y=0;y<24;y++){
            for(x=0;x<80;x++){
                move(y,x);
                if(WINDOW2[y][x]<=127)
                    addch(WINDOW2[y][x]);
                else{// no codepage 850 ;--;
                    if(WINDOW2[y][x]>=176 && WINDOW2[y][x]<=178)
                        addch(ACS_CKBOARD);
                    else if(WINDOW2[y][x]==106 || WINDOW2[y][x]==205)
                        addch(ACS_HLINE);
                    else if(WINDOW2[y][x]==179 || WINDOW2[y][x]==186)
                        addch(ACS_VLINE);
                    else if(WINDOW2[y][x]>=219 && WINDOW2[y][x]<=223)
                        addch(ACS_CKBOARD);
                    else if(WINDOW2[y][x]==254)
                        addch(ACS_CKBOARD);
                    else if(WINDOW2[y][x]==192 || WINDOW2[y][x]==200)
                        addch(ACS_LLCORNER);
                    else if(WINDOW2[y][x]==201 || WINDOW2[y][x]==218)
                        addch(ACS_ULCORNER);
                    else if(WINDOW2[y][x]==187 || WINDOW2[y][x]==191)
                        addch(ACS_URCORNER);
                    else if(WINDOW2[y][x]==188 || WINDOW2[y][x]==217)
                        addch(ACS_LRCORNER);
                    else if(WINDOW2[y][x]==204 || WINDOW2[y][x]==195)
                        addch(ACS_LTEE);
                    else if(WINDOW2[y][x]==180 || WINDOW2[y][x]==185)
                        addch(ACS_RTEE);
                    else if(WINDOW2[y][x]==193 || WINDOW2[y][x]==202)
                        addch(ACS_BTEE);
                    else if(WINDOW2[y][x]==194 || WINDOW2[y][x]==203)
                        addch(ACS_TTEE);
                    else if(WINDOW2[y][x]==197 || WINDOW2[y][x]==206)
                        addch(ACS_PLUS);
                    else if(WINDOW2[y][x]==167)
                        addch(ACS_DEGREE);
                    else if(WINDOW2[y][x]==241)
                        addch(ACS_PLMINUS);
                    else if(WINDOW2[y][x]==249 || WINDOW2[y][x]==250)
                        addch(ACS_BULLET);
                    else if(WINDOW2[y][x]==243)
                        addch(ACS_LEQUAL);
                    else if(WINDOW2[y][x]==242)
                        addch(ACS_GEQUAL);
                    else if(WINDOW2[y][x]==227)
                        addch(ACS_PI);
                    else
                        addch(32);
                }
            }
        }
     }

#endif
}

#ifdef linux
#if !defined(SDL_USE) &&  !defined(_WIN32)
static void finish(int sig){
    endwin();
    exit(0);
}
#endif
#endif

void goto_topscreen(){
	#ifdef SDL_USE
	#elif defined _WIN32
		COORD coord;
		coord.X = 0;
		coord.Y = 0;
		SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ),coord);
	#elif defined  __MSDOS__
		//no command for that yet
	#endif
}

void change_cursor_position(int line, int column){
	#ifdef _WIN32//WINDOWS FUNCTION TO CHANGE THE CURSOR POSITION
		#ifdef SDL_USE // no need for this code
		#else
			COORD coord;
			coord.X = column;
			coord.Y = line;
			SetConsoleCursorPosition(
			GetStdHandle( STD_OUTPUT_HANDLE ),coord);
		#endif
  #else
	//no ansi code found...
  #endif
}

void create_window_layout(int pos_x, int pos_y, int tam_x, int tam_y, bool shadow, bool title, bool option, char background, int window){
	int post_x=pos_x+1;
	int post_y=pos_y;
	while(post_x <tam_x+pos_x && post_x<MAX_HOR){
		if(window==DISPLAY_WINDOW1){
			WINDOW1[post_y][post_x]=205;
			WINDOW1[post_y+tam_y][post_x]=205;
		}else {
			WINDOW2[post_y][post_x]=205;
			WINDOW2[post_y+tam_y][post_x]=205;
		}post_x++;
	}
	post_x=pos_x;
	post_y=pos_y+1;
	while(post_y <tam_y+pos_y && post_y<MAX_VER){//Ok
		if(window==DISPLAY_WINDOW1){
			WINDOW1[post_y][post_x]=186;
			WINDOW1[post_y][post_x+tam_x]=186;
		}else {
			WINDOW2[post_y][post_x]=186;
			WINDOW2[post_y][post_x+tam_x]=186;
		}post_y++;
	}
	if(window==DISPLAY_WINDOW2){
		WINDOW2[pos_y][pos_x]=201; WINDOW2[pos_y][pos_x+tam_x]=187;
		WINDOW2[pos_y+tam_y][pos_x]=200; WINDOW2[pos_y+tam_y][pos_x+tam_x]=188;
	}else{
		WINDOW1[pos_y][pos_x]=201; WINDOW1[pos_y][pos_x+tam_x]=187;
		WINDOW1[pos_y+tam_y][pos_x]=200; WINDOW1[pos_y+tam_y][pos_x+tam_x]=188;
	}
	if(shadow){
		post_x=pos_x+tam_x+1;
		post_y=pos_y+1;

		if(post_x<MAX_HOR){
			while(post_y<=pos_y+tam_y+1 && post_y<MAX_VER){
				if(window==DISPLAY_WINDOW2)	WINDOW2[post_y][post_x]=176;
				else			WINDOW1[post_y][post_x]=176;
				post_y++;
			}
		}
		post_x=pos_x+1;
		post_y=pos_y+tam_y+1;
		if(post_y<MAX_VER){
			while(post_x<=pos_x+tam_x+1 && post_x<MAX_HOR){
				if(window==DISPLAY_WINDOW2)	WINDOW2[post_y][post_x]=176;
				else			WINDOW1[post_y][post_x]=176;
				post_x++;
			}
		}
	}
	post_x=pos_x+1;
	post_y=pos_y+1;
	while(post_y<pos_y+tam_y && post_y<=MAX_VER){//Transfere window1 para window2
		while(post_x<pos_x+tam_x && post_x<MAX_HOR){
			if(window==DISPLAY_WINDOW2)WINDOW2[post_y][post_x]=background;
			else WINDOW1[post_y][post_x]=background;
			post_x++;
		}
		post_y++;
		post_x=pos_x+1;
	}
	if(title){
		post_y=pos_y+2;
		post_x=pos_x+1;
		while(post_x<pos_x+tam_x && post_x<MAX_HOR){
			if(window==DISPLAY_WINDOW2)WINDOW2[post_y][post_x]=205;
			else WINDOW1[post_y][post_x]=205;
			post_x++;
		}
	}
	if(option){
		post_y=pos_y+tam_y-2;
		post_x=pos_x+1;
		while(post_x<pos_x+tam_x && post_x<MAX_HOR){
			if(window==DISPLAY_WINDOW2)WINDOW2[post_y][post_x]=205;
			else WINDOW1[post_y][post_x]=205;
			post_x++;
		}
	}

}

void transfer_window1_to_window2(){
int i=0;
int j=0;
	while(i<MAX_VER-1	){
		while(j<MAX_HOR){
			WINDOW2[i][j]=WINDOW1[i][j];
			j++;
		}
		i++;
		j=0;
	}
}

void transfer_window2_to_window1(){
int i=0;
int j=0;
	while(i<MAX_VER-1	){
		while(j<MAX_HOR){
			WINDOW1[i][j]=WINDOW2[i][j];
			j++;
		}
		i++;
		j=0;
	}
}

void insert_string_on_display(char *x, int pos_ver, int pos_hor, bool which){
	int i=strlen(x);
	int j=0;
	while(j<i){
		if(pos_hor>MAX_HOR)break;
//		if(x[j]!=' '){
			if(which==DISPLAY_WINDOW1)WINDOW1[pos_ver][pos_hor]=x[j];
			else WINDOW2[pos_ver][pos_hor]=x[j];
//		}
	j++;
		pos_hor++;
	}
}


void insert_number_on_display(int number, int pos_y,int pos_x, bool display){
	// Based on a generic itoa code without the null termination
	int base=10;
	unsigned char *str=0;
	unsigned char buffer=0;
	int i = 0;
	int j=0;
    bool isNegative = false;
	if(display)
		str=&WINDOW1[pos_y][pos_x];
	else
		str=&WINDOW2[pos_y][pos_x];


    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (number == 0)
    {
        str[i++] = '0';
        return ;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (number < 0 && base == 10)
    {
        isNegative = true;
        number = -number;
    }

    // Process individual digits
    while (number != 0)
    {
        int rem = number % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        number = number/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';


    // Reverse the string
	i--;
	while(j<i){
		buffer=str[j];// pega inicio
		str[j]=str[i];// inicio recebe final
		str[i]=buffer;// final recebe inicio
		j++;
		i--;
	}
}
